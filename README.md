##Nail
https://nail.od.ua/

Разработчик: Марциненко Александр

###Установка проекта

1. Запустить композер для устновки зависимостей
```
   composer install
```

2. Запустить yarn для установки зависимостей
```
   yarn install
```

3. Запустить webpack
```
   yarn encore dev
```
```
   yarn encore dev --watch
```
4. Создать env.local с настройками для dev окружения

###Зависимости
База данных

MASTER
```
   tesla.cityhost.com.ua:3306/ch19056636_nail_master
```

DEV
```
   tesla.cityhost.com.ua:3306/ch19056636_nail_dev
```