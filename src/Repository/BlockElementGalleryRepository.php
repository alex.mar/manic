<?php

namespace App\Repository;

use App\Entity\BlockElementGallery;
use App\Entity\Images;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BlockElementGallery|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlockElementGallery|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlockElementGallery[]    findAll()
 * @method BlockElementGallery[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlockElementGalleryRepository extends ServiceEntityRepository
{
    public const FIELD_ID = 'id';
    public const FIELD_ORDER = 'order';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BlockElementGallery::class);
    }

    public function getAllGalleryImages(): array
    {
        return $this->createQueryBuilder('beg')
            ->select('beg.id', 'i.path')
            ->join(Images::class, 'i', Join::WITH, 'i.id = beg.imageId')
            ->orderBy('beg.order')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param BlockElementGallery[] $elementEntities
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setElementEntities(array $elementEntities): void
    {
        if (empty($elementEntities)) {
            return;
        }

        foreach ($elementEntities as $elementEntity) {
            $this->_em->persist($elementEntity);
        }

        $this->_em->flush();
    }

    /**
     * @param BlockElementGallery[] $elementEntities
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function removeEntities(array $elementEntities): void
    {
        if (empty($elementEntities)) {
            return;
        }

        foreach ($elementEntities as $elementEntity) {
            $this->_em->remove($elementEntity);
        }

        $this->_em->flush();
    }
}
