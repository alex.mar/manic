<?php

namespace App\Repository;

use App\Entity\Blocks;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Blocks|null find($id, $lockMode = null, $lockVersion = null)
 * @method Blocks|null findOneBy(array $criteria, array $orderBy = null)
 * @method Blocks[]    findAll()
 * @method Blocks[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlocksRepository extends ServiceEntityRepository
{
    public const FIELD_ID = 'id';
    public const FIELD_TITLE = 'title';
    public const FIELD_DESCRIPTION = 'description';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Blocks::class);
    }

    /**
     * @param Blocks $block
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateBlock(Blocks $block): void
    {
        $this->_em->persist($block);
        $this->_em->flush();
    }

    /**
     * @return Blocks[]
     */
    public function getAllBlocks(): array
    {
        return $this->createQueryBuilder('b')
            ->select()
            ->orderBy('b.order')
            ->getQuery()
            ->getResult();
    }
}
