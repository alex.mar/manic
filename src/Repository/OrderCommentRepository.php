<?php

namespace App\Repository;

use App\Entity\OrderComment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrderComment|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderComment|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderComment[]    findAll()
 * @method OrderComment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderCommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderComment::class);
    }

    /**
     * @param int $orderId
     * @param string $comment
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setComment(int $orderId, string $comment): void
    {
        $orderComment = new OrderComment();
        $orderComment->setOrderId($orderId);
        $orderComment->setComment($comment);

        $this->_em->persist($orderComment);
        $this->_em->flush();
    }

    public function getCommentByOrderId(int $orderId): string
    {
        $comment = '';
        try {
            $comment = $this->createQueryBuilder('oc')
                ->select('oc.comment')
                ->where('oc.orderId = :orderId')
                ->setParameter('orderId', $orderId)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException $e) {
        } catch (NonUniqueResultException $e) {
        }

        return $comment;
    }
}
