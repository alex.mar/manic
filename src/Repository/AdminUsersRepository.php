<?php

namespace App\Repository;

use App\Entity\AdminUsers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AdminUsers|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdminUsers|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdminUsers[]    findAll()
 * @method AdminUsers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdminUsersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdminUsers::class);
    }

    /**
     * @param int $userId
     * @return string|null
     */
    public function getSessionKeyByUserId(int $userId): ?string
    {
        try {
            $result = $this->createQueryBuilder('au')
                ->select('au.sessionIndex')
                ->where('au.id = :userId')
                ->setParameter('userId', $userId)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException $e) {
            $result = null;
        } catch (NonUniqueResultException $e) {
            $result = null;
        }
        return $result;
    }

    /**
     * @param string $email
     * @param string $password
     * @return int
     */
    public function getIdByEmailAndPassword(string $email, string $password): ?int
    {
        try {
            $result = (int) $this->createQueryBuilder('au')
                ->select('au.id')
                ->where('au.email = :email')
                ->andWhere('au.password = :password')
                ->setParameters([
                    'email' => $email,
                    'password' => $password
                ])
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException $e) {
            $result = null;
        } catch (NonUniqueResultException $e) {
            $result = null;
        }

        return $result;
    }

    /**
     * @param int $userId
     * @param string $sessionKey
     */
    public function updateSessionKeyByUserId(int $userId, string $sessionKey): void
    {
        $this->createQueryBuilder('au')
            ->update(AdminUsers::class, 'au')
            ->set('au.sessionIndex', ':sessionKey')
            ->where('au.id = :userId')
            ->setParameters([
                'sessionKey' => $sessionKey,
                'userId' => $userId
            ])
            ->getQuery()
            ->execute();
    }
}
