<?php

namespace App\Repository;

use App\Entity\Meta;
use App\Entity\Urls;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Urls|null find($id, $lockMode = null, $lockVersion = null)
 * @method Urls|null findOneBy(array $criteria, array $orderBy = null)
 * @method Urls[]    findAll()
 * @method Urls[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UrlsRepository extends ServiceEntityRepository
{
    public const FIELD_ID = 'id';

    public const OPEN_VALUE_YES = 1;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Urls::class);
    }

    public function getUrlsWithoutMeta(): array
    {
        return $this->createQueryBuilder('u')
            ->select('u.id', 'u.url')
            ->leftJoin(Meta::class, 'm', Join::WITH, 'm.urlId = u.id')
            ->where('u.open = :open')
            ->andWhere('m.id IS NULL')
            ->setParameter('open', self::OPEN_VALUE_YES)
            ->getQuery()
            ->getResult();
    }

    public function getUrlIdByUrl(string $url): int
    {
        $qb = $this->createQueryBuilder('u')
            ->select('u.id')
            ->where('u.url = :url')
            ->setParameter('url', $url);

        $urlData = $qb->getQuery()->getResult();

        return !empty($urlData) ? $urlData[0][self::FIELD_ID] : 0;
    }
}
