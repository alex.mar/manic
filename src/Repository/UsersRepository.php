<?php

namespace App\Repository;

use App\Entity\Users;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Users|null find($id, $lockMode = null, $lockVersion = null)
 * @method Users|null findOneBy(array $criteria, array $orderBy = null)
 * @method Users[]    findAll()
 * @method Users[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Users::class);
    }

    /**
     * @param string $name
     * @param string $surname
     * @param string $phone
     * @return int
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setNewUser(string $name, string $surname, string $phone): int
    {
        $user = new Users();
        $user->setName($name);
        $user->setSurname($surname);
        $user->setPhone($phone);

        $this->_em->persist($user);
        $this->_em->flush();

        return $user->getId();
    }

    /**
     * @param string $phone
     * @return Users|null
     * @throws NonUniqueResultException
     */
    public function getUserByPhone(string $phone): ?Users
    {
        return $this->createQueryBuilder('u')
            ->select()
            ->where('u.phone = :phone')
            ->setParameter('phone', $phone)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param Users $user
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteOrderEntity(Users $user): void
    {
        $this->_em->remove($user);
        $this->_em->flush();
    }
}
