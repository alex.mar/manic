<?php

namespace App\Repository;

use App\Entity\BlockElementPrices;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BlockElementPrices|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlockElementPrices|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlockElementPrices[]    findAll()
 * @method BlockElementPrices[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlockElementPricesRepository extends ServiceEntityRepository
{
    public const FIELD_ID = 'id';
    public const FIELD_TITLE = 'title';
    public const FIELD_PRICE = 'price';
    public const FIELD_ORDER = 'order';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BlockElementPrices::class);
    }

    public function getAllPrices(): array
    {
        return $this->createQueryBuilder('bep')
            ->select()
            ->orderBy('bep.order')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param BlockElementPrices[] $elementEntities
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateEntities(array $elementEntities): void
    {
        if (empty($elementEntities)) {
            return;
        }

        foreach ($elementEntities as $elementEntity) {
            $this->_em->persist($elementEntity);
        }

        $this->_em->flush();
    }

    /**
     * @param BlockElementPrices[] $elementEntities
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function removeEntities(array $elementEntities): void
    {
        if (empty($elementEntities)) {
            return;
        }

        foreach ($elementEntities as $elementEntity) {
            $this->_em->remove($elementEntity);
        }

        $this->_em->flush();
    }
}
