<?php

namespace App\Repository;

use App\Entity\ImagesFolder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ImagesFolder|null find($id, $lockMode = null, $lockVersion = null)
 * @method ImagesFolder|null findOneBy(array $criteria, array $orderBy = null)
 * @method ImagesFolder[]    findAll()
 * @method ImagesFolder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImagesFolderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ImagesFolder::class);
    }

    /**
     * @param int|null $id
     * @param string $name
     * @param string $displayName
     * @param string $link
     * @param int $parentId
     * @param int $level
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setImagesFolder(?int $id, string $name, string $displayName, string $link, int $parentId, int $level): void
    {
        if ($id !== null) {
            $imageSection = $this->find($id);
        } else {
            $imageSection = new ImagesFolder();
        }

        $imageSection->setName($name);
        $imageSection->setDisplayName($displayName);
        $imageSection->setLink($link);
        $imageSection->setParentId($parentId);
        $imageSection->setLevel($level);

        $this->_em->persist($imageSection);
        $this->_em->flush();
    }

    /**
     * @param string $name
     * @return ImagesFolder
     * @throws NonUniqueResultException
     */
    public function getImagesFolderByName(string $name): ?ImagesFolder
    {
        return $this->createQueryBuilder('if')
            ->select()
            ->where('if.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
