<?php

namespace App\Repository;

use App\Entity\Meta;
use App\Entity\Urls;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Meta|null find($id, $lockMode = null, $lockVersion = null)
 * @method Meta|null findOneBy(array $criteria, array $orderBy = null)
 * @method Meta[]    findAll()
 * @method Meta[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MetaRepository extends ServiceEntityRepository
{
    public const FIELD_ID = 'id';
    public const FIELD_DISPLAY_NAME = 'displayName';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Meta::class);
    }

    /**
     * @param int $urlId
     * @param string $displayName
     * @return int
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createNewMeta(int $urlId, string $displayName): int
    {
        $meta = new Meta();
        $meta->setUrlId($urlId);
        $meta->setDisplayName($displayName);
        $meta->setTitle('');
        $meta->setDescription('');
        $meta->setKeywords('');

        $this->_em->persist($meta);
        $this->_em->flush();

        return $meta->getId();
    }

    public function getAllMetaList(): array
    {
        return $this->createQueryBuilder('m')
            ->select('m.id', 'm.displayName')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $metaId
     * @return array
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getMetaById(int $metaId): array
    {
        return $this->createQueryBuilder('m')
            ->select('m.id', 'm.displayName', 'm.title', 'm.keywords', 'm.description', 'u.url')
            ->join(Urls::class, 'u', Join::WITH, 'u.id = m.urlId')
            ->where('m.id = :metaId')
            ->setParameter('metaId', $metaId)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * @param int $urlId
     * @return Meta|null
     */
    public function getMetaByUrlId(int $urlId): ?Meta
    {
        $metaData = $this->createQueryBuilder('m')
            ->select()
            ->join(Urls::class, 'u', Join::WITH, 'u.id = m.urlId')
            ->where('m.urlId = :urlId')
            ->setParameter('urlId', $urlId)
            ->getQuery()
            ->getResult();

        return !empty($metaData) ? $metaData[0] : null;
    }

    /**
     * @param Meta $meta
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setMeta(Meta $meta): void
    {
        $this->_em->persist($meta);
        $this->_em->flush();
    }
}
