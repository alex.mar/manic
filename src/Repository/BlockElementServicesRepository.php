<?php

namespace App\Repository;

use App\Entity\BlockElementServices;
use App\Entity\Images;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BlockElementServices|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlockElementServices|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlockElementServices[]    findAll()
 * @method BlockElementServices[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlockElementServicesRepository extends ServiceEntityRepository
{
    public const FIELD_ID = 'id';
    public const FIELD_TITLE = 'title';
    public const FIELD_DESCRIPTION = 'description';
    public const FIELD_ORDER = 'order';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BlockElementServices::class);
    }

    public function getAllElements(): array
    {
        return $this->createQueryBuilder('bes')
            ->select('bes.id', 'bes.title', 'bes.description', 'bes.order', 'i.path as image', 'i.alt', 'i.id as imageId')
            ->leftJoin(Images::class, 'i', Join::WITH, 'bes.imageId = i.id')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param BlockElementServices[] $elementEntities
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setElementEntities(array $elementEntities): void
    {
        foreach ($elementEntities as $elementEntity) {
            $this->_em->persist($elementEntity);
        }

        $this->_em->flush();
    }
}
