<?php

namespace App\Repository;

use App\Entity\Images;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Images|null find($id, $lockMode = null, $lockVersion = null)
 * @method Images|null findOneBy(array $criteria, array $orderBy = null)
 * @method Images[]    findAll()
 * @method Images[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImagesRepository extends ServiceEntityRepository
{
    public const FIELD_ID = 'id';
    public const FIELD_ALT = 'alt';
    public const FIELD_PATH = 'path';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Images::class);
    }

    /**
     * @param Images[] $imagesEntity
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setImages(array $imagesEntity): void
    {
        foreach ($imagesEntity as $imageEntity) {
            $this->_em->persist($imageEntity);
        }

        $this->_em->flush();
    }

    /**
     * @param int $folderId
     * @param int $limit
     * @param int $offset
     * @return Images[]
     */
    public function getImagesByFolderId(int $folderId, int $limit = 0, int $offset = 0): array
    {
        $qb = $this->createQueryBuilder('i')
            ->select()
            ->where('i.folderId = :folderId')
            ->orderBy('i.created', 'DESC')
            ->addOrderBy('i.id', 'DESC')
            ->setParameter('folderId', $folderId);
        if ($offset > 0) {
            $qb->setFirstResult($offset);
        }
        if ($limit > 0) {
            $qb->setMaxResults($limit);
        }
        return $qb->getQuery()
            ->getResult();
    }

    /**
     * @param array $groupedAltAndImageId
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateAltByImageIds(array $groupedAltAndImageId): void
    {
        $imagesAltByImageId = [];
        foreach ($groupedAltAndImageId as $item) {
            $imagesAltByImageId[$item[self::FIELD_ID]] = $item[self::FIELD_ALT];
        }
        $images = $this->findBy([self::FIELD_ID => array_keys($imagesAltByImageId)]);
        foreach ($images as $image) {
            if (isset($imagesAltByImageId[$image->getId()])) {
                $image->setAlt($imagesAltByImageId[$image->getId()]);
            }
            $this->_em->persist($image);
        }

        $this->_em->flush();
    }

    /**
     * @param int[] $imageIds
     */
    public function deleteImageByIds(array $imageIds): void
    {
        $this->createQueryBuilder('i')
            ->delete()
            ->where('i.id IN (:imageIds)')
            ->setParameter('imageIds', $imageIds)
            ->getQuery()
            ->execute();
    }

    /**
     * @param int $folderId
     * @return int
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getCountAllImagesByFolderId(int $folderId): int
    {
        return $this->createQueryBuilder('i')
            ->select('COUNT(i.id) as count')
            ->where('i.folderId = :folderId')
            ->setParameter('folderId', $folderId)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function searchImages(string $word): array
    {
        return $this->createQueryBuilder('i')
            ->select('i.id, i.path')
            ->where('i.path LIKE :word')
            ->setParameter('word', $word. '%')
            ->getQuery()
            ->getResult();
    }

    public function getImageIdsByPath(array $imagesPath): array
    {
        if (empty($imagesPath)) {
            return [];
        }
        return $this->createQueryBuilder('i')
            ->select('i.id', 'i.path')
            ->where('i.path IN (:path)')
            ->setParameter('path', $imagesPath)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Images $images
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateEntity(Images $images): void
    {
        $this->_em->persist($images);
        $this->_em->flush();
    }

    /**
     * @param int $folderId
     * @param int $limit
     * @param array $excludeImageIds
     * @return array
     */
    public function getImagesByFolderIdWithExcludeImages(int $folderId, int $limit, array $excludeImageIds = []): array
    {
        return $this->createQueryBuilder('i')
            ->select()
            ->where('i.folderId = :folderId')
            ->andWhere('i.id NOT IN (:excludeImageIds)')
            ->setMaxResults($limit)
            ->orderBy('i.created', 'DESC')
            ->addOrderBy('i.id', 'DESC')
            ->setParameters([
                'folderId' => $folderId,
                'excludeImageIds' => $excludeImageIds
            ])
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $folderId
     * @return int
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getLastImageIdByFolderId(int $folderId): int
    {
        $imageId = $this->createQueryBuilder('i')
            ->select('i.id')
            ->where('i.folderId = :folderId')
            ->orderBy('i.created')
            ->addOrderBy('i.id')
            ->setMaxResults(1)
            ->setParameter('folderId', $folderId)
            ->getQuery()
            ->getSingleScalarResult();

        return (int) $imageId;
    }

    /**
     * @return int[]
     */
    public function getAllImageYears(): array
    {
        $result = $this->createQueryBuilder('i')
            ->distinct()
            ->select("YEAR(i.created) as year")
            ->orderBy('i.created', 'DESC')
            ->getQuery()
            ->getResult();
        return array_column($result, 'year', 'year');
    }
}
