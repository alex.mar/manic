<?php

namespace App\Command;

use App\Repository\ImagesFolderRepository;
use App\Repository\ImagesRepository;
use App\Services\Images\ImagesService;
use App\Services\Images\ImagesUploadServiceInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class ActualizeImagesCommand extends Command
{
    private const ROOT_IMAGES_FOLDER_PATH = '/public/images/';

    protected static $defaultName = 'app:actualize-images';

    /** @var ImagesFolderRepository */
    private $imagesFolderRepository;

    /** @var ImagesRepository */
    private $imagesRepository;

    /** @var ImagesService */
    private $imagesService;

    /** @var ImagesUploadServiceInterface */
    private $imagesUploadService;

    /** @var ParameterBagInterface */
    private $parameter;

    public function __construct(
        ImagesFolderRepository $imagesFolderRepository,
        ImagesRepository $imagesRepository,
        ImagesService $imagesService,
        ImagesUploadServiceInterface $imagesUploadService,
        ParameterBagInterface $parameter
    ) {
        $this->imagesFolderRepository = $imagesFolderRepository;
        $this->imagesRepository = $imagesRepository;
        $this->imagesService = $imagesService;
        $this->imagesUploadService = $imagesUploadService;
        $this->parameter = $parameter;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Actualizing images')
            ->setHelp('This command actualize original and media images ...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '',
            'Start actualize original images',
            '==============================='
        ]);
        $output->writeln($this->actualizeOriginalImages());
        $output->writeln([
            '',
            'Start actualize media images',
            '============================'
        ]);
        $output->writeln($this->actualizeMediaImages());


        $output->writeln([
            '',
            'Actualizing finished!',
            ''
        ]);

        return 0;
    }

    private function actualizeOriginalImages(): ?\Generator
    {
//        test commit
        $filesystem = new Filesystem();
        $existImagesDirectories = $this->getExistImagesDirectories();

        $folders = $this->imagesFolderRepository->findAll();
        foreach ($folders as $folder) {
            yield 'Start actualize folder ' . $folder->getName();
            if (!isset($existImagesDirectories[$folder->getName()])) {
                yield 'Directory no exist!';
                continue;
            }
            $images = $this->imagesRepository->getImagesByFolderId($folder->getId());
            $finder = new Finder();
            $files = $finder->files()->in(
                $this->parameter->get('kernel.project_dir')
                . self::ROOT_IMAGES_FOLDER_PATH
                . $folder->getName()
                . '/'
            );

            $toDeleteFromDirectory = [];
            $imageIdsToDeleteFromDB = [];

            $existImages = [];
            foreach ($files as $file) {
                $imageName = $file->getBasename();
                $existImages[$imageName] = $imageName;
            }
            $allowedImages = [];
            foreach ($images as $image) {
                $path = $image->getPath();
                $pathParts = explode('/', $path);
                $imageName = end($pathParts);
                if (!isset($existImages[$imageName])) {
                    $imageIdsToDeleteFromDB[] = $image->getId();
                }
                $allowedImages[$imageName] = $imageName;
            }
            foreach ($files as $file) {
                $imageName = $file->getBasename();
                if (!isset($allowedImages[$imageName])) {
                    $toDeleteFromDirectory[] = $file->getRealPath();
                }
            }

            if (!empty($toDeleteFromDirectory)) {
                $filesystem->remove($toDeleteFromDirectory);
            }
            if (!empty($imageIdsToDeleteFromDB)) {
                $this->imagesRepository->deleteImageByIds($imageIdsToDeleteFromDB);
            }

            $countImages = count($allowedImages);
            yield 'Actualized: ' . $countImages . ' images'
                .' | Deleted : ' . count($toDeleteFromDirectory)
                .' | Deleted rows: ' . count($imageIdsToDeleteFromDB);
        }
    }

    private function actualizeMediaImages(): ?\Generator
    {
        $images = $this->imagesRepository->findAll();
        $countImages = count($images);
        $allowedImageNames = [];
        foreach ($images as $image) {
            $imagePath = $image->getPath();
            $allowedImageNames[] = $this->imagesService->getMediaImageName($imagePath, ImagesService::SMALL_SIZE);
            $allowedImageNames[] = $this->imagesService->getMediaImageName($imagePath, ImagesService::MEDIUM_SIZE);
            $allowedImageNames[] = $this->imagesService->getMediaImageName($imagePath, ImagesService::HEIGHTEN_SIZE);
        }
        $allowedImageNames = array_combine($allowedImageNames, $allowedImageNames);

        $finder = new Finder();
        $files = $finder->files()->in(
            $this->parameter->get('kernel.project_dir')
            . DIRECTORY_SEPARATOR . 'public'
            . DIRECTORY_SEPARATOR . ImagesService::MAIN_IMAGE_FOLDER
            . DIRECTORY_SEPARATOR . ImagesService::MEDIA_IMAGE_FOLDER
            . DIRECTORY_SEPARATOR
        );
        $toDelete = [];
        foreach ($files as $file) {
            $imageName = $file->getBasename();
            if (!isset($allowedImageNames[$imageName])) {
                $toDelete[] = $file->getRealPath();
            }
        }
        if (!empty($toDelete)) {
            $filesystem = new Filesystem();
            $filesystem->remove($toDelete);
        }

        $uploaded = $this->imagesUploadService->uploadMediaImages($images);

        yield 'Actualized: ' . $countImages . ' images'
            .' | Deleted : ' . count($toDelete)
            .' | Created: ' . $uploaded;
    }

    private function getExistImagesDirectories(): array
    {
        $finder = new Finder();
        $existDirectories = [];
        $directories = $finder->directories()->in(
            $this->parameter->get('kernel.project_dir')
            . self::ROOT_IMAGES_FOLDER_PATH
        );
        foreach ($directories as $directory) {
            $directoryName = $directory->getBasename();
            $existDirectories[$directoryName] = $directoryName;
        }

        return $existDirectories;
    }
}
