<?php

namespace App\Controller;

use App\Services\Blocks\BlockServiceInterface;
use App\Services\MetaProvider\MetaProviderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

class IndexController extends AbstractController
{
    /** @var Request */
    private $request;

    /** @var MetaProviderInterface */
    private $metaProvider;

    /** @var BlockServiceInterface */
    private $blockService;

    /**
     * AjaxController constructor.
     * @param RequestStack $request
     * @param MetaProviderInterface $metaProvider
     * @param BlockServiceInterface $blockService
     */
    public function __construct(
        RequestStack $request,
        MetaProviderInterface $metaProvider,
        BlockServiceInterface $blockService
    ) {
        $this->request = $request->getCurrentRequest();
        $this->metaProvider = $metaProvider;
        $this->blockService = $blockService;
    }

    /**
     * @return Response
     */
    public function index(): Response
    {
        $meta = $this->metaProvider->provideMeta($this->request->getRequestUri());

        $blocks = $this->blockService->getAllBlocks();

        return $this->render('base.html.twig', [
            'meta' => $meta,
            'blocks' => $blocks
        ]);
    }
}
