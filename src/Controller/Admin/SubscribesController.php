<?php

namespace App\Controller\Admin;

use App\Repository\SubscribesRepository;
use App\Services\Authorization\AuthorizationServiceInterface;
use App\Services\Menu\MenuServiceInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class SubscribesController extends BaseController
{
    /** @var SubscribesRepository */
    private $subscribesRepository;

    /**
     * SubscribesController constructor.
     * @param RequestStack $request
     * @param Environment $template
     * @param AuthorizationServiceInterface $authorizationService
     * @param MenuServiceInterface $menuService
     * @param SubscribesRepository $subscribesRepository
     */
    public function __construct(
        RequestStack $request,
        Environment $template,
        AuthorizationServiceInterface $authorizationService,
        MenuServiceInterface $menuService,
        SubscribesRepository $subscribesRepository
    ) {
        $this->subscribesRepository = $subscribesRepository;

        parent::__construct($request, $template, $authorizationService, $menuService);
    }

    public function subscribes(): Response
    {
        $subscribes = $this->subscribesRepository->findAll();
        return $this->render('admin/subscribes/subscribes.html.twig', [
            'subscribes' => $subscribes
        ]);
    }
}
