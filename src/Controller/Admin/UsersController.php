<?php

namespace App\Controller\Admin;

use App\Repository\OrdersRepository;
use App\Repository\OrderPaymentRepository;
use App\Repository\UsersRepository;
use App\Services\Authorization\AuthorizationServiceInterface;
use App\Services\Menu\MenuServiceInterface;
use Exception;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class UsersController extends BaseController
{
    /** @var UsersRepository */
    private $usersRepository;

    /** @var OrdersRepository */
    private $ordersRepository;

    /** @var OrderPaymentRepository */
    private $orderPaymentRepository;

    /**
     * OrdersController constructor.
     * @param RequestStack $request
     * @param Environment $template
     * @param AuthorizationServiceInterface $authorizationService
     * @param MenuServiceInterface $menuService
     * @param UsersRepository $usersRepository
     * @param OrdersRepository $ordersRepository
     * @param OrderPaymentRepository $orderPaymentRepository
     */
    public function __construct(
        RequestStack $request,
        Environment $template,
        AuthorizationServiceInterface $authorizationService,
        MenuServiceInterface $menuService,
        UsersRepository $usersRepository,
        OrdersRepository $ordersRepository,
        OrderPaymentRepository $orderPaymentRepository
    ) {
        $this->usersRepository = $usersRepository;
        $this->ordersRepository = $ordersRepository;
        $this->orderPaymentRepository = $orderPaymentRepository;

        parent::__construct($request, $template, $authorizationService, $menuService);
    }

    public function users(): Response
    {
        $users = $this->usersRepository->findAll();
        return $this->render('admin/users/users.html.twig', [
            'users' => $users
        ]);
    }

    /**
     * @return Response
     * @throws Exception
     */
    public function user(): Response
    {
        $userId = (int) $this->request->get('userId');
        $user = $this->usersRepository->find($userId);
        if ($user === null) {
            throw new \RuntimeException('User not found');
        }
        $countOrders = $this->ordersRepository->getCountOrdersByUserId($userId);
        $firstOrder = $this->ordersRepository->getFirstOrderDateByUserId($userId);
        $lastOrder = $this->ordersRepository->getLastOrderDateByUserId($userId);
        $nextOrder = $this->ordersRepository->getNextOrderDateByUserId($userId);
        $countPayments = $this->orderPaymentRepository->getCountPaymentsByUserId($userId);
        $userData = [
            'id' => $user->getId(),
            'name' => $user->getName(),
            'surname' => $user->getSurname(),
            'phone' => $user->getPhone(),
            'countOrders' => $countOrders,
            'firstOrder' => $firstOrder,
            'lastOrder' => $lastOrder,
            'nextOrder' => $nextOrder,
            'countPayments' => $countPayments
        ];

        return $this->render('admin/users/user.html.twig', [
            'user' => $userData
        ]);
    }
}
