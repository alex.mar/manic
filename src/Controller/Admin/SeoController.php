<?php

namespace App\Controller\Admin;

use App\Repository\MetaRepository;
use App\Repository\UrlsRepository;
use App\Services\Authorization\AuthorizationServiceInterface;
use App\Services\Menu\MenuServiceInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class SeoController extends BaseController
{
    public const URL_KEY_META_ID = 'metaId';

    /** @var UrlsRepository */
    private $urlsRepository;

    /** @var MetaRepository */
    private $metaRepository;

    /**
     * SeoController constructor.
     * @param RequestStack $request
     * @param Environment $template
     * @param AuthorizationServiceInterface $authorizationService
     * @param MenuServiceInterface $menuService
     * @param UrlsRepository $urlsRepository
     * @param MetaRepository $metaRepository
     */
    public function __construct(
        RequestStack $request,
        Environment $template,
        AuthorizationServiceInterface $authorizationService,
        MenuServiceInterface $menuService,
        UrlsRepository $urlsRepository,
        MetaRepository $metaRepository
    ) {
        $this->urlsRepository = $urlsRepository;
        $this->metaRepository = $metaRepository;

        parent::__construct($request, $template, $authorizationService, $menuService);
    }

    /**
     * @return Response
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function seo(): Response
    {
        $metaId = (int) strip_tags($this->request->get(self::URL_KEY_META_ID));

        $metaTitle = '';
        $meta = [];

        $urls = $this->urlsRepository->getUrlsWithoutMeta();
        $pagesList = $this->metaRepository->getAllMetaList();
        foreach ($pagesList as $index => $item) {
            $isActive = false;
            if ($metaId === $item[MetaRepository::FIELD_ID]) {
                $isActive = true;
                $metaTitle = $item[MetaRepository::FIELD_DISPLAY_NAME];
            }
            $pagesList[$index]['link'] = $this->request->getPathInfo() . '?' . self::URL_KEY_META_ID . '=' . $item[MetaRepository::FIELD_ID];
            $pagesList[$index]['isActive'] = $isActive;
        }

        if (!empty($metaId)) {
            $meta = $this->metaRepository->getMetaById($metaId);
        }

        return $this->render('admin/seo/seo.html.twig', [
            'urls' => $urls,
            'pagesList' => $pagesList,
            'metaTitle' => $metaTitle,
            'meta' => $meta
        ]);
    }
}
