<?php

namespace App\Controller\Admin;

use Symfony\Component\HttpFoundation\Response;

class IndexController extends BaseController
{
    public function index(): Response
    {
        return $this->render('admin/base.html.twig');
    }
}
