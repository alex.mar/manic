<?php

namespace App\Controller\Admin;

use App\Form\UploadImagesType;
use App\Services\Authorization\AuthorizationServiceInterface;
use App\Services\Images\Folder\Folder;
use App\Services\Images\ImagesService;
use App\Services\Images\ImagesUploadServiceInterface;
use App\Services\Menu\MenuServiceInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class ImagesController extends BaseController
{
    public const LIMIT_BATCH_IMAGES = 1000;

    /** @var ImagesService */
    private $imagesService;

    /** @var ImagesUploadServiceInterface */
    private $imagesUploadService;

    /**
     * ImagesController constructor.
     * @param RequestStack $request
     * @param Environment $template
     * @param AuthorizationServiceInterface $authorizationService
     * @param MenuServiceInterface $menuService
     * @param ImagesService $imagesService
     * @param ImagesUploadServiceInterface $imagesUploadService
     */
    public function __construct(
        RequestStack $request,
        Environment $template,
        AuthorizationServiceInterface $authorizationService,
        MenuServiceInterface $menuService,
        ImagesService $imagesService,
        ImagesUploadServiceInterface $imagesUploadService
    ) {
        $this->imagesService = $imagesService;
        $this->imagesUploadService = $imagesUploadService;

        parent::__construct($request, $template, $authorizationService, $menuService);
    }

    public function images(): Response
    {
        ini_set('memory_limit', '512M');
        $token = $this->request->get('token');
        $token = trim($token, '/');

        $folders = $this->imagesService->getAllImageFolders($this->urlData);

        $form = $this->createForm(UploadImagesType::class);
        $form->handleRequest($this->request);

        $images = [];
        $folderTitle = '';
        $currentFolder = null;
        $uploadedImages = [];
        $showAltPopup = false;
        $countEmptyImages = 0;
        $loadButton = ['show' => true];

        if (!empty($token)) {
            $urlFolders = explode('/', $token);
            $lastFolder = end($urlFolders);
            $folderTitle = $lastFolder;
            $currentFolder = $this->imagesService->getImagesFolderByName($lastFolder);
            if ($currentFolder instanceof Folder) {
                $folderTitle = $currentFolder->getDisplayName();
            }
        } elseif (!empty($folders)) {
            $currentFolder = $folders[0];
            $folderTitle = $currentFolder->getDisplayName();
        }

        if ($currentFolder instanceof Folder) {
            if ($form->isSubmitted() && $form->isValid()) {
                $uploadImages = $form->get('images')->getData();
                if ($uploadImages) {
                    /** @var UploadedFile $uploadImage */
                    foreach ($uploadImages as $uploadImage) {
                        $fullPath = $currentFolder->getPath() . $uploadImage->getClientOriginalName();
                        $this->imagesUploadService->uploadImage($fullPath, '', $currentFolder->getId());
                        $uploadImage->move($currentFolder->getPath(), $uploadImage->getClientOriginalName());
                    }
                    $uploadedImages = $this->imagesUploadService->send();
                    $this->imagesService->applyMediaSize($uploadedImages, ImagesService::SMALL_SIZE);
                    $showAltPopup = true;
                    $this->addFlash('success', 'Фото успешно добавлены!');
                }
            }
            $images = $this->imagesService->getImagesByFolderId($currentFolder->getId(), self::LIMIT_BATCH_IMAGES);
            $images = $this->imagesService->applyMediaSize($images, ImagesService::MEDIUM_SIZE);
            if (!empty($images)) {
                $countEmptyImages = $this->imagesService->calculateEmptyImages(count($images));
                $countAllImages = $this->imagesService->getCountAllImagesByFolderId($currentFolder->getId());
                if (count($images) === $countAllImages) {
                    $loadButton['show'] = false;
                }
            }
        }

        return $this->render('admin/images/images.html.twig', [
            'folders' => $folders,
            'folderTitle' => $folderTitle,
            'images' => $images,
            'countEmptyImages' => $countEmptyImages,
            'form' => $form->createView(),
            'loadButton' => $loadButton,
            'uploadedImages' => $uploadedImages,
            'showAltPopup' => $showAltPopup
        ]);
    }
}
