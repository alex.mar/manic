<?php

namespace App\Controller\Admin;

use App\Services\Authorization\AuthorizationServiceInterface;
use App\Services\Menu\MenuServiceInterface;
use App\Services\UrlData\AdminUrlData;
use App\Services\UrlData\UrlDataInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Environment;

class BaseController extends AbstractController
{
    /** @var Environment */
    protected $template;

    /** @var Request */
    protected $request;

    /** @var UrlDataInterface */
    protected $urlData;

    public function __construct(
        RequestStack $request,
        Environment $template,
        AuthorizationServiceInterface $authorizationService,
        MenuServiceInterface $menuService
    ) {
        $this->request = $request->getCurrentRequest();
        $this->template = $template;

        $this->urlData = new AdminUrlData($this->request);

        $authorizationService->checkAuth($this->request);
        $menu = $menuService->loadMenu($this->request);

        $this->template->addGlobal('menu', $menu);
    }
}
