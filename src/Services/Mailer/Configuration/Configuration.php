<?php

namespace App\Services\Mailer\Configuration;

class Configuration
{
    /** @var string */
    private $transport;

    /** @var string */
    private $host;

    /** @var string */
    private $authMode;

    /** @var int */
    private $port;

    /** @var string */
    private $encryption;

    /** @var string  */
    private $user;

    /** @var string  */
    private $password;

    public function __construct(
        string $transport,
        string $host,
        string $authMode,
        int $port,
        string $encryption,
        string $user,
        string $password
    ) {
        $this->transport = $transport;
        $this->host = $host;
        $this->authMode = $authMode;
        $this->port = $port;
        $this->encryption = $encryption;
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getTransport(): string
    {
        return $this->transport;
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @return string
     */
    public function getAuthMode(): string
    {
        return $this->authMode;
    }

    /**
     * @return int
     */
    public function getPort(): int
    {
        return $this->port;
    }

    /**
     * @return string
     */
    public function getEncryption(): string
    {
        return $this->encryption;
    }

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}
