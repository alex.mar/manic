<?php

namespace App\Services\Translit;

interface TranslitServiceInterface
{
    public function translit(string $string): string;
}
