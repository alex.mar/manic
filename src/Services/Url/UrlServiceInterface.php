<?php

namespace App\Services\Url;

interface UrlServiceInterface
{
    public static function formatUri(string $url): string;

    public static function formatTableUri(string $url): string;

    public function formatFullUrl(string $url): string;
}
