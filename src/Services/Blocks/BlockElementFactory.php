<?php

namespace App\Services\Blocks;

use App\Services\Blocks\BlockElement\BlockElementAbout;
use App\Services\Blocks\BlockElement\BlockElementGallery;
use App\Services\Blocks\BlockElement\BlockElementInterface;
use App\Services\Blocks\BlockElement\BlockElementPrices;
use App\Services\Blocks\BlockElement\BlockElementServices;
use App\Services\Blocks\BlockElement\EmptyBlockElement;

class BlockElementFactory implements BlockElementFactoryInterface
{
    /** @var BlockElementInterface[] */
    private $blockElements;

    /**
     * BlockElementFactory constructor.
     * @param BlockElementServices $blockElementServices
     * @param BlockElementGallery $blockElementGallery
     * @param BlockElementAbout $blockElementAbout
     * @param BlockElementPrices $blockElementPrices
     */
    public function __construct(
        BlockElementServices $blockElementServices,
        BlockElementGallery $blockElementGallery,
        BlockElementAbout $blockElementAbout,
        BlockElementPrices $blockElementPrices
    ) {
        $this->blockElements = [
            $blockElementServices,
            $blockElementGallery,
            $blockElementAbout,
            $blockElementPrices
        ];
    }

    /**
     * @param string $blockName
     * @return BlockElementInterface
     */
    public function build(string $blockName): BlockElementInterface
    {
        foreach ($this->blockElements as $blockElement) {
            if ($blockElement->getKey() === $blockName) {
                return $blockElement;
            }
        }

        return new EmptyBlockElement();
    }
}
