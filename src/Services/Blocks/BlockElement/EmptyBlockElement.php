<?php

namespace App\Services\Blocks\BlockElement;

use App\Services\Blocks\BlockItem\BlockItem;

class EmptyBlockElement implements BlockElementInterface
{
    public function getKey(): string
    {
        return 0;
    }

    public function loadContent(BlockItem $blockItem, bool $isAdmin = false): void
    {
    }
}
