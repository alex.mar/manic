<?php

namespace App\Services\Blocks\BlockElement;

use App\Services\Blocks\BlockItem\BlockItem;

interface BlockElementInterface
{
    public function getKey(): string;

    public function loadContent(BlockItem $blockItem, bool $isAdmin = false): void;
}
