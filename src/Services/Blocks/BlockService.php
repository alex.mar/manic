<?php

namespace App\Services\Blocks;

use App\Entity\Blocks;
use App\Repository\BlocksRepository;
use App\Services\Blocks\BlockItem\BlockItem;

class BlockService implements BlockServiceInterface
{
    /** @var BlockElementFactoryInterface */
    private $blockElementFactory;

    /** @var BlocksRepository */
    private $blocksRepository;

    /**
     * BlockService constructor.
     * @param BlockElementFactoryInterface $blockElementFactory
     * @param BlocksRepository $blocksRepository
     */
    public function __construct(
        BlockElementFactoryInterface $blockElementFactory,
        BlocksRepository $blocksRepository
    ) {
        $this->blockElementFactory = $blockElementFactory;
        $this->blocksRepository = $blocksRepository;
    }

    public function getAllBlocks(): array
    {
        $blocks = $this->blocksRepository->getAllBlocks();
        $blockItems = $this->buildBlockItemsFromEntities($blocks);
        foreach ($blockItems as $blockItem) {
            $this->loadBlockContent($blockItem);
        }

        return $blockItems;
    }

    public function loadBlockContent(BlockItem $blockItem, bool $isAdmin = false): void
    {
        $blockElement = $this->blockElementFactory->build($blockItem->getName());
        $blockElement->loadContent($blockItem, $isAdmin);
    }

    /**
     * @param Blocks[] $blocks
     * @return BlockItem[]
     */
    public function buildBlockItemsFromEntities(array $blocks): array
    {
        $blockItems = [];
        foreach ($blocks as $block) {
            $blockItem = new BlockItem($block->getId());
            $blockItem->setName($block->getName())
                ->setDisplayName($block->getDisplayName())
                ->setTitle($block->getTitle() ?? '')
                ->setDescription($block->getDescription() ?? '');
            $blockItems[] = $blockItem;
        }

        return $blockItems;
    }
}
