<?php

namespace App\Services\Images\Image;

class Image
{
    /** @var int */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $originalPath;

    /** @var string */
    private $mediaPath;

    /** @var string */
    private $alt;

    /** @var int */
    private $order;

    /** @var string */
    private $created;

    /** @var int */
    private $year;

    /**
     * Image constructor.
     * @param int $id
     * @param string $name
     * @param string $originalPath
     * @param string $mediaPath
     * @param string $alt
     * @param int $order
     * @param string $created
     * @param int $year
     */
    public function __construct(
        int $id,
        string $name,
        string $originalPath,
        string $mediaPath,
        string $alt,
        int $order,
        string $created,
        int $year
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->originalPath = $originalPath;
        $this->mediaPath = $mediaPath;
        $this->alt = $alt;
        $this->order = $order;
        $this->created = $created;
        $this->year = $year;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setOriginalPath(string $originalPath): void
    {
        $this->originalPath = $originalPath;
    }

    public function getOriginalPath(): string
    {
        return $this->originalPath;
    }

    public function getMediaPath(): string
    {
        return $this->mediaPath;
    }

    public function setMediaPath(string $mediaPath): void
    {
        $this->mediaPath = $mediaPath;
    }

    public function getAlt(): string
    {
        return $this->alt;
    }

    public function getOrder(): int
    {
        return $this->order;
    }

    public function setOrder(int $order): void
    {
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getCreated(): string
    {
        return $this->created;
    }

    /**
     * @param string $created
     */
    public function setCreated(string $created): void
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getYear(): int
    {
        return $this->year;
    }

    /**
     * @param int $year
     */
    public function setYear(int $year): void
    {
        $this->year = $year;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return get_object_vars($this);
    }
}
