<?php

namespace App\Services\Images\Folder;

class Folder
{
    /** @var int|null */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $displayName;

    /** @var string */
    private $link;

    /** @var string */
    private $path;

    /** @var bool */
    private $isActive;

    /**
     * Section constructor.
     * @param int|null $id
     * @param string $name
     * @param string $displayName
     * @param string $link
     * @param string $path
     * @param bool $isActive
     */
    public function __construct(?int $id, string $name, string $displayName, string $link, string $path, bool $isActive)
    {
        $this->id = $id;
        $this->name = $name;
        $this->displayName = $displayName;
        $this->link = $link;
        $this->path = $path;
        $this->isActive = $isActive;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }
}
