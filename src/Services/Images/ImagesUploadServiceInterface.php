<?php

namespace App\Services\Images;

use App\Entity\Images;
use App\Services\Images\Image\Image;

interface ImagesUploadServiceInterface
{
    /**
     * @param string $path
     * @param string $alt
     * @param int $folderId
     */
    public function uploadImage(string $path, string $alt, int $folderId): void;

    /**
     * @param Images[] $imageEntities
     * @param bool $forceSave
     * @return int
     */
    public function uploadMediaImages(array $imageEntities, bool $forceSave = false): int;

    /**
     * @return Image[]
     */
    public function send(): array;
}
