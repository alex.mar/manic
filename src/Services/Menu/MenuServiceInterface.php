<?php

namespace App\Services\Menu;

use Symfony\Component\HttpFoundation\Request;

interface MenuServiceInterface
{
    /**
     * @param Request $request
     * @return array
     */
    public function loadMenu(Request $request): array;
}
