<?php

namespace App\Services\Authorization;

use Symfony\Component\HttpFoundation\Request;

interface AuthorizationServiceInterface
{
    public function checkAuth(Request $request): void;

    /**
     * @param string $email
     * @param string $password
     * @return int|null
     */
    public function checkIssetUser(string $email, string $password): ?int;

    /**
     * @param int $userId
     * @return string
     */
    public function generateSessionKey(int $userId): string;
}
