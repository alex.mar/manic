<?php

namespace App\Services\UrlData;

interface UrlDataInterface
{
    public function getBaseUrl(): string;

    public function getCurrentUri(): string;
}
