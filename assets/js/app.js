const $ = require('jquery');
global.$ = global.jQuery = $;
global.$ = $;
require('@fortawesome/fontawesome-free/js/fontawesome.min');
require('@fortawesome/fontawesome-free/css/all.min.css');
require('jquery.maskedinput/src/jquery.maskedinput');
require('owl.carousel2/dist/assets/owl.carousel.min.css');
require('owl.carousel2/dist/owl.carousel.min');
require('tail.datetime/css/tail.datetime-default-orange.min.css');
require('../libs/js/modal-loading/modal-loading');
require('../libs/css/modal-loading/modal-loading.css');
require('../libs/css/modal-loading/modal-loading-animate.css');
require('magnific-popup/dist/jquery.magnific-popup.min');
require('magnific-popup/dist/magnific-popup.css');

import '../css/style.css';
import '../css/media.css';

export const STATUS_SUCCESS = 'success';
const STATUS_ERROR = 'error';

$('.mobile-tab').hide();

let loading;

export function loadingOn()
{
    loading = new Loading({
        loadingPadding: 8,
        loadingBorderRadius: 4,
        animationWidth: 35,
        animationHeight: 35
    });
}

export function loadingOff()
{
    loading.out();
}

$(document).ready(function () {
    $('.owl-carousel').owlCarousel({
        items: 1
    });

    $('header').removeClass('default');
    $(window).scroll(function () {
        if ($(this).scrollTop() > 20) {
            $('header').addClass('default').fadeIn('fast');
        } else {
            $('header').removeClass('default').fadeIn('fast');
        }
    });

    $(".header-nav, .mobile-tab, .footer-menu-list").on("click","a", function (event) {
        let id  = $(this).attr('href');
        if ($(id).length) {
            event.preventDefault();
            let top = $(id).offset().top;
            $('body,html').animate({scrollTop: top}, 1000);
            let mobileTab = $('.mobile-tab');
            if (mobileTab.is(':visible')) {
                mobileTab.slideUp({
                    duration: 300,
                    easing: 'linear'
                });
            }
        }
    });

    $(".intro-inner").on("click","a.btn-main-order", function (event) {
        event.preventDefault();
        let id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);
    });

    $('#menu').on("click", function () {
        $('.mobile-tab').slideToggle();
    });

    $(document).mouseup(function (e) {
        let header = $('header');
        if (!header.is(e.target) && header.has(e.target).length === 0) {
            let mobileTab = $('.mobile-tab');
            if (mobileTab.is(':visible')) {
                mobileTab.slideUp({
                    duration: 300,
                    easing: 'linear'
                });
            }
            // $('.mobile-tab').slideUp();
        }
    });

    tail.DateTime(".tail-datetime-field", {
        position: "top",
        locale: "ru",
        dateFormat: 'YYYY-mm-dd',
        timeFormat: false,
        timeHours: false,
        timeMinutes: false,
        timeIncrement: false
    });

    let formInputOrder = $('.form-input');

    formInputOrder.focus(function () {
        $(this).parent().find('.form-label').css({
            'top': '-10px',
            'font-size': '12px'
        });
    });

    formInputOrder.focusout(function (e) {
        if ($(this).hasClass('tail-datetime-field')) {
            // if ($('.order-date').val() === '') {
            //     $(this).parent().find('.form-label').css({
            //         'top': '0',
            //         'font-size': '16px'
            //     });
            // }
        } else if ($(this).val() === '' || $(this).val() === '+38 (0__) __-__-___') {
            $(this).parent().find('.form-label').css({
                'top': '0',
                'font-size': '16px'
            });
        }
    });

    $('#order-phone').mask('+38 (099) 99-99-999', {
        placeholder:"+38 (0__) __-__-___",
        autoclear: false
    });

    $('.footer-mobile-title').on('click', function () {
        $(this).siblings('.footer-mobile-content').slideToggle();
    });

    $('.btn-form-order').on('click', function () {
        let inputName = $('.order-name');
        let inputPhone = $('.order-phone');
        let inputDate = $('.order-date');

        let errorName = validateOrderField(inputName);
        let errorPhone = validateOrderField(inputPhone);
        let errorDate = validateOrderField(inputDate);

        if (errorName || errorPhone || errorDate) {
            return false;
        } else {
            loadingOn();
            $.ajax({
                url: '/order/',
                dataType: 'json',
                type: 'POST',
                data: {
                    name: inputName.val(),
                    phone: inputPhone.val(),
                    date: inputDate.val(),
                },
                success: function (response) {
                    loadingOff();
                    if (response.status === STATUS_SUCCESS) {
                        $.magnificPopup.open({
                            items: {
                                src: '#popup-success-order',
                            },
                            closeBtnInside:true
                        });
                    } else {
                        $.magnificPopup.open({
                            items: {
                                src: '#popup-error-order',
                            },
                            closeBtnInside:true
                        });
                    }
                },
                error: function () {
                    loadingOff();
                    $.magnificPopup.open({
                        items: {
                            src: '#popup-error-order',
                        },
                        closeBtnInside:true
                    });
                }
            });

            return false;
        }
    });

    formInputOrder.on('keyup', function () {
        if ($(this).hasClass('has-error')) {
            $(this).removeClass('has-error');
            $(this).prev('.field-order-error').fadeOut('slow');
            $(this).animate({
                borderBottomColor:"#696969",
                borderLeftColor:"#696969",
                borderRightColor:"#696969",
                borderTopColor:"#696969"
            }, 1000);
        }
    });

    $('.btn-feedback').on('click', function () {
        let inputName = $(this).parent('.form-footer-feedback').find('.feedback-name');
        let inputEmail = $(this).parent('.form-footer-feedback').find('.feedback-email');
        let inputText = $(this).parent('.form-footer-feedback').find('.feedback-text');

        let errorName = validateFeedbackField(inputName);
        if (errorName) {
            return false;
        }
        let errorEmail = validateFeedbackField(inputEmail);
        if (errorEmail) {
            return false;
        }
        let errorText = validateFeedbackField(inputText);
        if (errorText) {
            return false;
        }

        loadingOn();
        $.ajax({
            url: '/feedback/',
            dataType: 'json',
            type: 'POST',
            data: {
                name: inputName.val(),
                email: inputEmail.val(),
                text: inputText.val(),
            },
            success: function (response) {
                loadingOff();
                if (response.status === STATUS_SUCCESS) {
                    $('.btn-feedback').parent('.form-footer-feedback').find('.feedback-notice')
                        .text('Ваш отзыв успешно отправлен!').css('color', '#96b732').fadeIn(300);
                } else {
                    $('.btn-feedback').parent('.form-footer-feedback').find('.feedback-notice')
                        .text('Ошибка! Попробуйте позже').fadeIn(300);
                }
            },
            error: function () {
                loadingOff();
                $('.btn-feedback').parent('.form-footer-feedback').find('.feedback-notice')
                    .text('Ошибка! Попробуйте позже').fadeIn(300);
            }
        });

        return false;
    });

    $('.btn-subscribe').on('click', function () {
        let email = $(this).parent('.form-subscribe').find('.subscribe-email').val();
        if (email === '') {
            $(this).parent('.form-subscribe').find('.notice-subscribe')
                .text('Введите свой email!').fadeIn(300);
            return false;
        }

        let regEmail = /^[a-z0-9_-]+@[a-z0-9-]+\.[a-z]{2,6}$/i;
        if (!regEmail.test(email)) {
            $(this).parent('.form-subscribe').find('.notice-subscribe')
                .text('Введите настоящий email!').fadeIn(300);
            return false;
        }

        $(this).parent('.form-subscribe').find('.notice-subscribe').fadeOut(300);

        loadingOn();
        $.ajax({
            url: '/subscribe/',
            dataType: 'json',
            type: 'POST',
            data: {
                email: email
            },
            success: function (response) {
                loadingOff();
                if (response.status === STATUS_SUCCESS) {
                    $('.btn-subscribe').parent('.form-subscribe').find('.notice-subscribe')
                        .text('Вы успешно подписаны!').css('color', '#96b732').fadeIn(300);
                } else {
                    $('.btn-subscribe').parent('.form-subscribe').find('.notice-subscribe')
                        .text('Ошибка! Попробуйте позже').fadeIn(300);
                }
            },
            error: function () {
                loadingOff();
                $('.btn-subscribe').parent('.form-subscribe').find('.notice-subscribe')
                    .text('Ошибка! Попробуйте позже').fadeIn(300);
            }
        });

        return false;
    });

    $('.gallery-item').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        }
    });

    let topMenu = $(".header-nav");
    let topMenuHeight = topMenu.outerHeight()+15;
    let menuItems = topMenu.find("a");
    let scrollItems = menuItems.map(function(){
            let item = $($(this).attr("href"));
            if (item.length) { return item; }
        });

    $(window).on('scroll', function () {
        let fromTop = $(this).scrollTop() + topMenuHeight;
        let cur = scrollItems.map(function(){
            if ($(this).offset().top < fromTop)
                return this;
        });
        cur = cur[cur.length-1];
        let id = cur && cur.length ? cur[0].id : "";
        menuItems.removeClass("active").end().find(".nav-item[href='#"+id+"']").addClass("active");
    });
});

function validateOrderField(field)
{
    let value = field.val();
    let id = field.attr('id');
    let isError = false;
    let message = '';
    if (value === '') {
        message = 'Заполните это поле';
        isError = true;
    } else {
        switch (id) {
            case 'order-name':
                let regName = /^[a-zA-Zа-яА-Я]+$/;
                if (value.length < 2 || !regName.test(value)) {
                    message = 'Введите настоящее имя';
                    isError = true;
                }
                break;
            case 'order-phone':
                let regPhone = /^\+38\s\(0\d{2}\) \d{2}-\d{2}-\d{3}$/;
                if (!regPhone.test(value)) {
                    message = 'Введите настоящий телефон';
                    isError = true;
                }
                break;
            case 'order-date':
                let regDate = /^\d{4}[./-]\d{2}[./-]\d{2}$/;
                if (!regDate.test(value)) {
                    message = 'Введите дату в формате 0000-00-00';
                    isError = true;
                }
                break;
        }
    }

    if (isError) {
        field.addClass('has-error');
        field.prev('.field-order-error').text(message).fadeIn(500);
        field.css('border', '2px solid #ea3f3f');
        $(this).animate({
            borderBottomColor:"#ea3f3f",
            borderLeftColor:"#ea3f3f",
            borderRightColor:"#ea3f3f",
            borderTopColor:"#ea3f3f"
        }, 1000);
    } else {
        field.prev('.field-order-error').fadeOut('slow');
        field.css('border', '2px solid #696969');
        $(this).animate({
            borderBottomColor:"#696969",
            borderLeftColor:"#696969",
            borderRightColor:"#696969",
            borderTopColor:"#696969"
        }, 1000);
    }

    return isError;
}

function validateFeedbackField(field)
{
    let value = field.val();
    let id = field.attr('id');
    let isError = false;
    let message = '';
    if (value === '') {
        message = 'Заполните все поля';
        isError = true;
    } else {
        switch (id) {
            case 'feedback-name':
                let regName = /^[a-zA-Zа-яА-Я]+$/;
                if (value.length < 2 || !regName.test(value)) {
                    message = 'Введите настоящее имя';
                    isError = true;
                }
                break;
            case 'feedback-email':
                let regEmail = /^[a-z0-9_-]+@[a-z0-9-]+\.[a-z]{2,6}$/i;
                if (!regEmail.test(value)) {
                    message = 'Введите настоящий email';
                    isError = true;
                }
                break;
        }
    }

    if (isError) {
        field.addClass('has-error');
        field.parent('.form-footer-feedback').find('.feedback-notice').text(message).fadeIn(300);
    } else {
        field.parent('.form-footer-feedback').find('.feedback-notice').fadeOut(300);
    }

    return isError;
}