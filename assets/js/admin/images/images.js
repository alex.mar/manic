import '../app';

import '../../../css/admin/images/styles.css';
import '../../../css/admin/images/media.css';
import {loadingOff, loadingOn, STATUS_SUCCESS} from "../app";

$(document).ready(function () {

    showAltPopup();

    function showAltPopup()
    {
        if ($('#popup-alt-form').hasClass('show')) {
            $.magnificPopup.open({
                items: {
                    src: '#popup-alt-form',
                },
                closeBtnInside:true,
                closeOnBgClick: false,
                enableEscapeKey: false
            });
        }
    }

    $('.btn-create-folder').on('click', function () {
        $.magnificPopup.open({
            items: {
                src: '#popup-create-folder',
            },
            closeBtnInside:true
        });
    });

    $('.popup-btn-create-folder').on('click', function () {
        let folderName = $('.folder-name').val();
        if (folderName.length === 0) {
            $('#popup-create-folder form p.error').text('Введите имя папки');
            $('#popup-create-folder form p.label').css('margin-bottom', '0');
            $('#popup-create-folder form input').css('border', '1px solid #cc3636');
            return false;
        }
        loadingOn();
        $.ajax({
            url: '/admin/action/images/create-folder/',
            dataType: 'json',
            type: 'POST',
            data: {
                name: folderName
            },
            success: function (response) {
                if (response.status === 'success') {
                    let data = response.data;
                    $('.folder-menu ul').append('<li><a href="' + data.link + '"><i class="fas fa-folder"></i>' + folderName + '</a></li>');
                    $('.mfp-close').trigger("click");
                } else if (response.status === 'error') {
                    $('#popup-create-folder form p.error').text(response.message);
                    $('#popup-create-folder form p.label').css('margin-bottom', '0');
                    $('#popup-create-folder form input').css('border', '1px solid #cc3636');
                }
                loadingOff();
            },
            error: function () {
                loadingOff();
                // $.magnificPopup.open({
                //     items: {
                //         src: '#popup-error-order',
                //     },
                //     closeBtnInside:true
                // });
            }
        });

        return false;
    });

    $('.input-upload-images').change(function(){
        $(this).closest('form').trigger('submit');
    });

    // $('.btn-load-more').on('click', function () {
    //     let folderId = $('.folder-menu ul li a.active').data('folder-id');
    //     let num = $(".image-item").map(function() {
    //         return $(this).data('image-order');
    //     }).get();
    //
    //     let highestOrder = Math.max.apply(Math, num);
    //
    //     loadingOn();
    //     $.ajax({
    //         url: '/admin/action/images/load-more/',
    //         dataType: 'json',
    //         type: 'POST',
    //         data: {
    //             folderId: folderId,
    //             order: highestOrder
    //         },
    //         success: function (response) {
    //             $.each(response.data, function (index, image) {
    //                 // var basic = $('#demo-basic').croppie({
    //                 //     viewport: {
    //                 //         width: 150,
    //                 //         height: 150
    //                 //     },
    //                 //     showZoomer: false,
    //                 //     enableZoom: false
    //                 //     // boundary:{
    //                 //     //     width: 100,
    //                 //     //     height: 100
    //                 //     // }
    //                 // });
    //                 // basic.croppie('bind', {
    //                 //     url: image.path,
    //                 //     points: [0, 3024, 0, 3024]
    //                 // });
    //                 // let htmlImg = '<img class="my-image" src="' + image.path + '" alt="' + image.alt + '">';
    //                 // let cropImage = $(htmlImg).croppie({
    //                 //     viewport: {
    //                 //         width: 100,
    //                 //         height: 100
    //                 //     },
    //                 //     boundary:{
    //                 //         width: 100,
    //                 //         height: 100
    //                 //     }
    //                 // });
    //                 // cropImage.croppie('bind', {
    //                 //     url: image.path,
    //                 //     points: [0,100,0,100]
    //                 // });
    //                 // cropImage.croppie('result', 'canvas').then(function (src) {
    //                 //     console.log(src);
    //                 //     window.open(src);
    //                 // });
    //                 // console.log('end');
    //                 // cropImage.croppie('result', {
    //                 //     type: 'canvas',
    //                 //     size: 'viewport'
    //                 // }).then(function(html) {
    //                 //     console.log(html);
    //                 //     let cropHtmlImg = $(html).find('img');
    //                 //     console.log(cropHtmlImg);
    //                 //     let imageItem = '<div class="image-item" data-image-order="' + image.order + '">\n' +
    //                 //         html +
    //                 //         '                        <div class="image-item-menu">\n' +
    //                 //         '                            <span>' + image.name + '</span>\n' +
    //                 //         '                            <div class="image-item-menu-btn">\n' +
    //                 //         '                                <i class="fas fa-pencil-alt"></i>\n' +
    //                 //         '                                <i class="fas fa-trash"></i>\n' +
    //                 //         '                            </div>\n' +
    //                 //         '                        </div>\n' +
    //                 //         '                        <div class="image-item-description">\n' +
    //                 //         '                            <p><span>Alt:</span> ' + image.alt + '</p>\n' +
    //                 //         '                            <p><span>Размер:</span> 300Х600</p>\n' +
    //                 //         '                        </div>\n' +
    //                 //         '                    </div>';
    //                 //     $('.images-list').append($(html).html());
    //                 // });
    //             });
    //             loadingOff();
    //         },
    //         error: function () {
    //             loadingOff();
    //         }
    //     });
    //
    //     return false;
    // });

    $('.popup-save-alt').on('click', function () {
        let altData = [];
        $('.alt-form input').each(function(){
            let alt = $(this).val();
            if (alt.length === 0) {
                alert('Все поля должны быть заполнены!');
                return false;
            }
            altData.push({id: $(this).data('image-id'), alt: alt});
        });

        loadingOn();
        $.ajax({
            url: '/admin/action/images/save-alt/',
            dataType: 'json',
            type: 'POST',
            data: {
                altData: altData
            },
            success: function (response) {
                if (response.status === STATUS_SUCCESS) {
                    let altByImageId = {};
                    $.each(altData, function (index, value) {
                        altByImageId[value['id']] = value['alt'];
                    });
                    $('.images-list div.image-item').each(function(){
                        let imageId = $(this).data('image-id');
                        if (altByImageId[imageId] !== undefined) {
                            $(this).find('span.alt-text').text(altByImageId[imageId]);
                        }
                    });
                }
                loadingOff();
                $.magnificPopup.close();
                // alert(response.message);
            },
            error: function () {
                loadingOff();
                alert('Что то пошло не так! Попробуйте позже');
            }
        });

        return false;
    });

    $('.btn-delete-image').on('click', function () {
        let imageId = $(this).data('image-id');
        if (confirm('Вы уверены что хотите удалить фото?')) {
            loadingOn();
            $.ajax({
                url: '/admin/action/images/delete-image/',
                dataType: 'json',
                type: 'POST',
                data: {
                    imageId: imageId
                },
                success: function (response) {
                    if (response.status === STATUS_SUCCESS) {
                        $('.image-item[data-image-id=' + imageId + ']').remove();
                        loadingOff();
                        alert(response.message);
                    }
                },
                error: function () {
                    loadingOff();
                    alert('Что то пошло не так! Попробуйте позже');
                }
            });
        }
    });

    $('.btn-copy-path').on('click', function () {
        let value = $(this).data('image-path').replace(/^\/+/g, '');

        let temp = $("<input>");
        $("body").append(temp);
        temp.val(value).select();
        document.execCommand("copy");
        temp.remove();
        Tipped.create($(this), 'copied', {
            showOn: false
        });
        Tipped.show($(this));
    });

    $('.btn-edit-image').on('click', function () {
        let alt = $(this).parent().parent().next('.image-item-description').find('.alt-text').text();
        let imageId = $(this).data('image-id');
        $('.popup-input-alt').val(alt);
        $('.popup-input-image-id').val(imageId);
        $.magnificPopup.open({
            items: {
                src: '#popup-edit-image',
            },
            closeBtnInside:true
        });
    });

    $('.popup-btn-edit-image').on('click', function () {
        let imageData = [];
        $('.form-edit-image input').each(function(){
            let value = $(this).val();
            if (value.length === 0) {
                alert('Все поля должны быть заполнены!');
                return false;
            }
            let key = $(this).attr('name');
            imageData[key] = value;
        });

        loadingOn();
        $.ajax({
            url: '/admin/action/images/save-image-data/',
            dataType: 'json',
            type: 'POST',
            data: {
                imageData: Object.assign({}, imageData)
            },
            success: function (response) {
                if (response.status === STATUS_SUCCESS) {
                    $('.images-list').find('.image-item[data-image-id=' + imageData['id'] + ']')
                        .find('span.alt-text').text(imageData['alt']);
                }
                loadingOff();
                $.magnificPopup.close();
                alert(response.message);
            },
            error: function () {
                loadingOff();
                alert('Что то пошло не так! Попробуйте позже');
            }
        });

        return false;
    });

    $('.btn-rotate-image').on('click', function () {
        let imageId = $(this).data('image-id');
        let imagePath = $(this).data('image-path');
        loadingOn();
        $.ajax({
            url: '/admin/action/images/rotate-image/',
            dataType: 'json',
            type: 'POST',
            data: {
                imageId: imageId
            },
            success: function (response) {
                if (response.status === STATUS_SUCCESS) {
                    $('.image-item[data-image-id=' + imageId + ']').find('img').attr('src', imagePath + '?' + new Date().getTime());
                } else {
                    alert(response.message);
                }
                loadingOff();
            },
            error: function () {
                loadingOff();
                alert('Что то пошло не так! Попробуйте позже');
            }
        });
    });
});