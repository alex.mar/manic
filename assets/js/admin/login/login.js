import '../app';

import '../../../css/admin/login/styles.css';
import '../../../css/admin/login/media.css';
import {loadingOff, loadingOn} from "../app";

$(document).ready(function() {
    $('.btn-login').on('click', function () {
        let email = $('.user-email').val();
        let password = $('.user-password').val();

        let isError = false;
        let message = '';
        if (email === '' || password === '') {
            message = 'Заполните пустые поля!';
            isError = true;
        }

        if (isError) {
            $('p.notice-error').text(message).css('display', 'block');
            return false;
        }
        loadingOn();
        $.ajax({
            url: '/admin/login/',
            dataType: 'json',
            type: 'POST',
            data: {
                email: email,
                password: password,
                submit: true
            },
            success: function (response) {
                loadingOff();
                if (response.status === 'error') {
                    $('p.notice-error').text(response.message).css('display', 'block');
                } else if (response.status === 'success') {
                    window.location.href = response.url;
                }
                return false;
            },
            error: function () {
                loadingOff();
                $('p.notice-error').text('Ошибка! Попробуйте позже').css('display', 'block');
                return false;
            }
        });
        return false;
    });
});