import '../blocks';

import '../../../../css/admin/blocks/elements/services/styles.css';
import '../../../../css/admin/blocks/elements/services/media.css';

import {getAllInputValues, loadingOff, loadingOn, formatKeyValueArray} from "../../app";
import {inputImageAutocomplete} from "../blocks";

$(document).ready(function () {

    inputImageAutocomplete($('.input-image-autocomplete'));

    $('.btn-save-block-services').on('click', function () {
        let blockId = $(this).data('block-id');
        let headerData = $('.form-header-services:visible').serializeArray();
        headerData = formatKeyValueArray(headerData);
        let elementsData = [];
        $('.form-elements-services .element-item').each(function() {
            let elementId = $(this).data('element-id');
            let inputItems = $(this).find('input');
            let elementData = getAllInputValues(inputItems);
            elementData['id'] = elementId;
            let myObject = Object.assign({}, elementData);
            elementsData.push(myObject);
        });

        let blockData = {
            id: blockId,
            title: headerData['title'],
            description: headerData['description'],
            elements: elementsData
        };

        loadingOn();
        $.ajax({
            url: '/admin/action/blocks/services/save/',
            dataType: 'json',
            type: 'POST',
            data: {
                block: blockData
            },
            success: function (response) {
                loadingOff();
                alert(response.message);
            },
            error: function () {
                loadingOff();
                alert('Что то пошло не так! Попробуйте позже');
            }
        });

        return false;
    });
});