import '../blocks';

import '../../../../css/admin/blocks/elements/about/styles.css';
import '../../../../css/admin/blocks/elements/about/media.css';

import {inputImageAutocomplete} from "../blocks";
import {formatKeyValueArray, getAllInputValues, loadingOff, loadingOn} from "../../app";

$(document).ready(function () {

    inputImageAutocomplete($(".input-image-autocomplete"));

    $('.btn-save-block-about').on('click', function () {
        let blockId = $(this).data('block-id');
        let headerData = $('.form-header-about:visible').serializeArray();
        headerData = formatKeyValueArray(headerData);
        let elementData = [];
        $('.form-element-about table tr').each(function() {
            let item = $(this).find('input');
            if (item.length === 0) {
                item = $(this).find('textarea');
            }
            let key = item.attr('name');
            elementData[key] = item.val();
        });
        elementData['id'] = $('.form-element-about').data('element-id');

        let blockData = {
            id: blockId,
            title: headerData['title'],
            description: headerData['description'],
            element: Object.assign({}, elementData)
        };

        loadingOn();
        $.ajax({
            url: '/admin/action/blocks/about/save/',
            dataType: 'json',
            type: 'POST',
            data: {
                block: blockData
            },
            success: function (response) {
                let elementId = response.data.elementId;
                $('.form-element-about').data('element-id', elementId);
                loadingOff();
                alert(response.message);
            },
            error: function () {
                loadingOff();
                alert('Что то пошло не так! Попробуйте позже');
            }
        });

        return false;
    });

});