import '../blocks';

require('jquery-ui-sortable-npm/jquery-ui-sortable.min');

import '../../../../css/admin/blocks/elements/prices/styles.css';
import '../../../../css/admin/blocks/elements/prices/media.css';
import {formatKeyValueArray, getAllInputValues, loadingOff, loadingOn} from "../../app";

$(document).ready(function () {

    $('.sortable-table tbody').sortable();

    $('.btn-add-price').on('click', function () {
        $('.form-elements-prices table tbody').append('<tr>\n' +
            '                <td>\n' +
            '                    <input type="text" name="title"/>\n' +
            '                </td>\n' +
            '                <td>\n' +
            '                    <input type="text" name="price"/>\n' +
            '                </td>\n' +
            '                <td>\n' +
            '                    <div class="price-menu">\n' +
            '                        <p><i class="fas fa-trash btn-delete-price"></i></p>\n' +
            '                    </div>\n' +
            '                </td>\n' +
            '            </tr>');
    });

    $('.form-elements-prices table').on('click', '.btn-delete-price', function () {
        if (confirm('Вы уверены что хотите удалить price?')) {
            $(this).closest('tr').remove();
        }

        return false;
    });

    $('.btn-save-block-prices').on('click', function () {
        let blockId = $(this).data('block-id');
        let headerData = $('.form-header-prices:visible').serializeArray();
        headerData = formatKeyValueArray(headerData);
        let elementsData = [];
        let order = 0;
        $('.form-elements-prices table tbody tr').each(function() {
            let elementId = $(this).data('element-id') ? $(this).data('element-id') : null;
            let inputItems = $(this).find('input');
            let elementData = getAllInputValues(inputItems);
            elementData['id'] = elementId;
            elementData['order'] = order;
            let myObject = Object.assign({}, elementData);
            elementsData.push(myObject);
            order++;
        });

        let blockData = {
            id: blockId,
            title: headerData['title'],
            description: headerData['description'],
            elements: elementsData
        };

        loadingOn();
        $.ajax({
            url: '/admin/action/blocks/prices/save/',
            dataType: 'json',
            type: 'POST',
            data: {
                block: blockData
            },
            success: function (response) {
                loadingOff();
                alert(response.message);
            },
            error: function () {
                loadingOff();
                alert('Что то пошло не так! Попробуйте позже');
            }
        });

        return false;
    });

});