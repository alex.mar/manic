import '../app';

import '../../../css/admin/orders/styles.css';
import '../../../css/admin/orders/media.css';
import {loadingOff, loadingOn, STATUS_SUCCESS} from "../app";

$(document).ready(function () {

    $('.btn-delete-order').on('click', function () {
        let orderId = $(this).data('order-id');

        if (confirm('Вы уверены что хотите удалить запись?')) {
            loadingOn();
            $.ajax({
                url: '/admin/action/orders/delete-order/',
                dataType: 'json',
                type: 'POST',
                data: {
                    orderId: orderId
                },
                success: function (response) {
                    if (response.status === STATUS_SUCCESS) {
                        loadingOff();
                        alert(response.message);
                        window.location.href = window.location.origin + '/admin/orders/';
                    }
                },
                error: function () {
                    loadingOff();
                    alert('Что то пошло не так! Попробуйте позже');
                }
            });
        }

        return false;
    });

    $('.btn-edit-order').on('click', function () {
        let status = $(this).data('order-status');
        let orderId = $(this).data('order-id');
        $('#popup-edit-order form select option[value="' + status + '"]').prop('selected', true);
        $('.popup-btn-save-order').data('order-id', orderId);
        $.magnificPopup.open({
            items: {
                src: '#popup-edit-order',
            },
            closeBtnInside:true
        });
    });

    $('.selector-order-status').on('change', function () {
        let status = $(this).val();
        if (status === 'done') {
            $('.block-status-canceled').css('display', 'none');
            $('.block-status-done').css('display', 'block');
        } else if (status === 'canceled') {
            $('.block-status-done').css('display', 'none');
            $('.block-status-canceled').css('display', 'block');
        } else {
            $('.block-status-done').css('display', 'none');
            $('.block-status-canceled').css('display', 'none');
        }

        return false;
    });

    $('.popup-btn-save-order').on('click', function () {
        let btnSave = $('.popup-btn-save-order');
        let status = $('.selector-order-status').val();
        let orderId = btnSave.data('order-id');
        let payment = 0;
        let comment = '';
        let tdBgColor = '';
        if (status === 'done') {
            payment = $('.block-status-done input[name=payment]').val();
            payment = parseInt(payment) || 0;
            tdBgColor = '#ebffef';
        } else if (status === 'canceled') {
            comment = $('.block-status-canceled textarea').val();
            tdBgColor = '#fff0f0';
        } else {
            tdBgColor = '#ffffe8';
        }
        if (payment === 0 && comment.length === 0) {
            alert('Поля не должны быть пустыми');
            return false;
        }

        loadingOn();
        $.ajax({
            url: '/admin/action/orders/edit-order/',
            dataType: 'json',
            type: 'POST',
            data: {
                orderId: orderId,
                status: status,
                payment: payment,
                comment: comment
            },
            success: function (response) {
                if (response.status === STATUS_SUCCESS) {
                    $('.table-orders tbody tr[data-order-id="' + orderId + '"]').find('td.td-status').text(status).css('background-color', tdBgColor);
                    loadingOff();
                    alert(response.message);
                    $.magnificPopup.close();
                }
            },
            error: function () {
                loadingOff();
                alert('Что то пошло не так! Попробуйте позже');
            }
        });


        return false;
    });

    $('.btn-create-order').on('click', function () {
        $.magnificPopup.open({
            items: {
                src: '#popup-create-order',
            },
            closeBtnInside:true
        });
    });

    $('.popup-btn-create-order').on('click', function () {
        let userId = $("select[name=user]").val();
        let datetime = $("input[name=datetime]").val();
        if (datetime.length === 0) {
            $('#popup-create-order form p.error').text('Все поля должны быть заполнены');
            return false;
        } else {
            $('#popup-create-order form p.error').css('display', 'none');
        }
        loadingOn();
        $.ajax({
            url: '/admin/action/orders/create-order/',
            dataType: 'json',
            type: 'POST',
            data: {
                userId: userId,
                datetime: datetime
            },
            success: function (response) {
                if (response.status === 'success') {
                    $('.mfp-close').trigger("click");
                    alert(response.message);
                    window.location.href = window.location.origin + '/admin/orders/';
                } else if (response.status === 'error') {
                    $('#popup-create-order form p.error').text(response.message).css('display', 'block');
                }
                loadingOff();
            },
            error: function () {
                loadingOff();
                alert('Что то пошло не так. Попробуйте позже');
            }
        });

        return false;
    });

    (function () {
        'use strict';
        $('.input-file').each(function () {
            var $input = $(this),
                $label = $input.next('.js-labelFile'),
                labelVal = $label.html();

            $input.on('change', function (element) {
                var fileName = '';
                if (element.target.value) fileName = element.target.value.split('\\').pop();
                fileName ? $label.addClass('has-file').find('.js-fileName').html(fileName) : $label.removeClass('has-file').html(labelVal);
            });
        });

    })();

    $('.btn-save-order-report').on('click', function () {
        let status = $('.selector-order-status').val();
        let photo = null;
        let payment = 0;
        let comment = '';
        let alt = '';
        if (status === 'done') {
            payment = $('.block-status-done input[name=payment]').val();
            payment = parseInt(payment) || 0;
            photo = $('.block-status-done input[name=photo]').prop('files')[0];
            alt = $('.block-status-done input[name=alt]').val();
        } else if (status === 'canceled') {
            comment = $('.block-status-canceled textarea').val();
        }

        if ((payment === 0 || alt.length === 0 || photo === undefined) && comment.length === 0) {
            alert('Заполните все поля');
            return false;
        }

        return true;
    });
});